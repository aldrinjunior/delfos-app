import {NgModule} from '@angular/core';

import {ComponentRoutingModule} from './component-routing.module';
import {ComponentListComponent} from './components/component-list/component-list.component';
import {SharedModule} from '../shared/shared.module';
import { ComponentDetailComponent } from './components/component-detail/component-detail.component';
import {DialogConfirmComponent} from '../shared/components/dialog-confirm/dialog-confirm.component';

@NgModule({
  declarations: [ComponentListComponent, ComponentDetailComponent],
  imports: [
    SharedModule,
    ComponentRoutingModule
  ],
  entryComponents: [DialogConfirmComponent, ComponentDetailComponent]
})
export class ComponentModule { }
