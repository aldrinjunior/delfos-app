import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import {ComponentService} from '../../services/component.service';
import {ErrorService} from '../../../core/services/error.service';

import {Component as ComponentModel} from '../../../core/models/component.model';
import {catchError, finalize, tap} from 'rxjs/operators';


@Component({
  selector: 'app-component-detail',
  templateUrl: './component-detail.component.html',
  styleUrls: ['./component-detail.component.scss']
})
export class ComponentDetailComponent implements OnInit {

  component: ComponentModel = {};
  formGroup: FormGroup;

  configs = {
    inLoading: false,
    inSaving: false
  };

  constructor(
    @Optional() public dialogRef: MatDialogRef<ComponentDetailComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: ComponentDetailData,
    private formBuilder: FormBuilder,
    private componentService: ComponentService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }
    this.createForm();

    if (this.data.component) {
      this.component = this.data.component;

      this.configs.inLoading = true;
      this.componentService.findById(this.component.id)
        .pipe(
          tap((data) => {
            this.component = data;
            this.formGroup.patchValue({...this.component});
          }),
          catchError(this.errorService.handleError),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();

      this.formGroup.patchValue({...this.component});
    }

  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      description: [null],
      enabled: [false]
    });
  }

  onSubmit(): void {
    this.configs.inSaving = true;

    const model: ComponentModel = {
      ...this.component,
      ...this.formGroup.value
    };

    this.componentService.save(model, model.id)
      .pipe(
        tap((site) => {
          this.configs.inSaving = false;
          this.dialogRef.close({successful: true, data: model});
        }),
        catchError((error) => {
          this.configs.inSaving = false;
          return this.errorService.handleError(error);
        }),
        finalize( () => {
          this.configs.inSaving = false;
          this.configs.inLoading = false;
        })
      ).subscribe();

  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

}


export class ComponentDetailData {
  title: string;
  component: ComponentModel;
}
