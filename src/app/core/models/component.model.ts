export interface Component {

  id?: string;
  name?: string;
  description?: string;
  token?: string;
  enabled?: boolean;

}

export interface Token {
  id: string;
  token: string;
}
