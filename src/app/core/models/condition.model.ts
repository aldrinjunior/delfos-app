import {Rule} from './rule.model';
import {Field} from './field.model';

export class Condition {

  id?: string;
  operator?: string;
  conditions?: Condition[];
  parentId?: string;
  parent?: Condition;
  ruleId?: string;
  rule?: Rule;
  order?: number;
  expressionType?: number;
  value?: string;
  andOr?: string;
  type?: ConditionType | number;
  fieldId?: string;
  field?: Field;

  constructor() {

  }

}

export enum ConditionType {
  GROUP = 0,
  MESSAGE = 1,
  FIELD = 2
}

export enum MessageConditionType {
  CONDITION_NAME = 1,
  CONDITION_SYSTEM = 2,
  CONDITION_NIVEL = 3,
  CONDITION_REGISTRY_LOG = 4
}

export enum OperatorType {
  EQ = 'eq',
  NE = 'ne',
  LIKE = 'like',
  GT = 'gt',
  LT = 'lt',
  GTE = 'gte',
  LTE = 'lte',
  IN = 'in',
  BTW = 'btw',
  IS_NULL = 'isNull',
  IS_NOT_NUL = 'isNotNull',
  OR = 'or',
  AND = 'and',
  GP = 'gp'
}
