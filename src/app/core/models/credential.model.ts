export class Credential {

  constructor(
    public identifier: string,
    public password: string
  ) {}

}
