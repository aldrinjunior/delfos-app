import {Event} from './event.model';
import {Rule} from './rule.model';
import {DataFieldLog} from './data-field-log';
import {Message} from './message.model';
import {Component} from './component.model';

export interface EventLog {

  id?: string;
  eventId?: string;
  event?: Event;
  ruleId?: string;
  rule?: Rule;
  messageId?: string;
  message?: Message;
  dateHour?: number;
  startDelay?: number;
  hash?: string;
  componentId?: string;
  component?: Component;
  identifier?: string;

  dataFields?: DataFieldLog[];

}
