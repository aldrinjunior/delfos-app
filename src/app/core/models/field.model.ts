import {Message} from '@angular/compiler/src/i18n/i18n_ast';
import {FieldType} from './field-type.model';

export interface Field {

  id?: string;
  name?: string;
  description?: string;
  size?: number;
  decimalPoint?: boolean;
  precision?: number;
  messageId?: string;
  message?: Message;
  type?: FieldType | string;
  index?: number;
  valueFormat?: string;

}
