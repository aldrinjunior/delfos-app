export interface FilterDefinition {

  name: string;
  operator: string;
  value?: any;

}
