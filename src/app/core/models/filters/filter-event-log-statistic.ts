import {EventLog} from '../event-log.model';

export interface FilterEventLogStatistic {

  groupBy?: string[];
  sortBy?: string[];
  filters?: EventLog;
  limit?: number;

}
