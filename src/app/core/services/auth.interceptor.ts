import {Injectable} from '@angular/core';

import {AuthService} from './auth.service';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {StoragKeys} from '../../storege-keys';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,

  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (window.localStorage.getItem(StoragKeys.AUTH_TOKEN) !== null) {
      const httpRequest = req.clone({
        setHeaders: {
          'Authorization': `Bearer ${window.localStorage.getItem(StoragKeys.AUTH_TOKEN)}`
        }
       });
       return next.handle(httpRequest)
       .pipe(
        catchError((error) => {
          if (error.status === 401 || error.status === 403) {

            this.authService.logout();
            if (error.status === 401) {
              return throwError('Usuário não autenticado.');
            } else if (error.status === 403) {
              return throwError('Autenticação necessária ou credencias inválidas.');
            }

            return throwError(error);
          }
        return throwError(error);
        })
      );
    } else {
      return next.handle(req);
    }

  }

}
