import {Injectable} from '@angular/core';
import {Observable, of, ReplaySubject, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

import {ErrorService} from './error.service';
import {BaseService} from './base.service';
import {Credential} from '../models/credential.model';
import {StoragKeys} from '../../storege-keys';

import {catchError, take, tap} from 'rxjs/operators';
import {UserService} from '../../user/services/user.service';
import {User} from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  redirectUrl: string;
  keepLogin: boolean;

  private _isAuthenticated = new ReplaySubject<boolean>(1);
  private authUser: User;

  getUrl(): string {
    return `${this.getBasePath()}/login`;
  }

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private errorService: ErrorService
  ) {
    super();

    this.init();
  }

  init(): void {
    this.keepLogin = JSON.parse(window.localStorage.getItem(StoragKeys.KEEP_SIGNED));
  }

  get isAuthenticated(): Observable<boolean> {
    return this._isAuthenticated.asObservable();
  }

  get user(): User {
    return this.authUser;
  }

  login(credential: Credential): Observable<any> {
    return this.http.post(this.getUrl(), {...credential}, { observe: 'response' })
    .pipe(
      tap(res => {
        this.setAuthState({id: res.body.id, token: res && res.body.token, isAuthenticated: res !== null});
      },
      catchError((error) => {
        this.setAuthState({id: null, token: null, isAuthenticated: false});
        return this.errorService.handleError(error);
      })
    ));
  }

  logout(): void {
    window.localStorage.removeItem(StoragKeys.AUTH_TOKEN);
    window.localStorage.removeItem(StoragKeys.KEEP_SIGNED);
    this.keepLogin = false;
    this._isAuthenticated.next(false);
    this.router.navigate(['/login']);
  }

  toggleKeepLogin(): void {
    this.keepLogin = !this.keepLogin;
    window.localStorage.setItem(StoragKeys.KEEP_SIGNED, this.keepLogin.toString());
  }

  autoLogin(): Observable<any> {
    if (!this.keepLogin) {
      this._isAuthenticated.next(false);
      window.localStorage.removeItem(StoragKeys.KEEP_SIGNED);
      return of();
    }

    return this.http.post(`${this.getBasePath()}/api/auth/refresh/token`,
      null,
      {
        headers: {'Authorization': `Bearer ${window.localStorage.getItem(StoragKeys.AUTH_TOKEN)}`},
        observe: 'response'
      }).pipe(
      tap((res: any) => {
          this.setAuthState({id: res.body.id, token: res && res.body.token, isAuthenticated: res !== null});
        },
        catchError((error) => {
          this.setAuthState({id: null, token: null, isAuthenticated: false});
          return throwError(new Error('Token inválido! Login necessário.'));

          /*if (error.status === 403) {
            return of('Token inválido! Login necessário.');
          } else {
            return of(this.errorService.handleError(error));
          }*/

        })
      ));

  }

  changePasswordCurrentUser(password: string, newpassword: string): Observable<any> {
      return this.http.post(`${this.getBasePath()}/api/auth/change/password`,
        {password: password, newPassword: newpassword});
  }

  private setAuthState(authData: {id: string, token: string, isAuthenticated: boolean}): void {
    if (authData.isAuthenticated) {
      window.localStorage.setItem(StoragKeys.AUTH_TOKEN, authData.token);

      this._isAuthenticated.next(authData.isAuthenticated);

      if (authData.id != null) {
        this.setAuthUser(authData.id)
          .pipe(
            take(1),
            tap(() => this._isAuthenticated.next(authData.isAuthenticated))
          ).subscribe();
      }

      return;
    }
    this._isAuthenticated.next(authData.isAuthenticated);
  }

  private setAuthUser(userId: string): Observable<User> {
    return this.userService.findById(userId)
      .pipe(
        tap((user: User) => {
          this.authUser = user;
        })
      );
  }

}
