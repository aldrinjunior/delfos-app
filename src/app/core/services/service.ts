import {BaseService} from './base.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Page} from '../models/page.model';
import {FilterDefinition} from '../models/filters/filter-definition';
import {QueryDefinition} from '../models/filters/query-definition';

export abstract class Service<T> extends BaseService {

  private http: HttpClient;

  protected constructor(
    private endpointUrl: string,
    http: HttpClient
  ) {
    super();
    this.http = http;
  }

  protected get url(): string {
    return `${this.getBasePath()}${this.endpointUrl}`;
  }

  findById(id: string): Observable<T> {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  findDetailById(id: string): Observable<T> {
    return this.http.get<T>(`${this.url}/detail/${id}`);
  }

  save(data: T, id?: any): Observable<T> {
    if (id) {
      return this.http.put<T>(`${this.url}/${id}`, data);
    } else {
      return this.http.post<T>(this.url, data);
    }
  }

  delete(id: string): Observable<any> {
    if (id !== null) {
      return this.http.delete<any>(`${this.url}/${id}`);
    }
    return throwError('invalid ID');
  }

  findAll(): Observable<T[]> {
    return this.http.get<T[]>(`${this.url}/all`);
  }

  find(data?: T, size?: number | 20, page?: number | 0): Observable<Page> {
    let params = new HttpParams()
      .set('page', page ? page.toString() : '0')
      .set('size', size ? size.toString() : '20');

    if (data != null) {
      const keys = Object.keys(data);
      keys.forEach(name => {
        if (data[name] != null) {
          params = params.set(name, data[name]);
        }
      });
    }
    return this.http.get<T>(`${this.url}`,
      {
        params: params
      });
  }

  filter(filters?: FilterDefinition[], size?: number | 20, page?: number | 0,
         options?: {method: 'GET' | 'POST'}): Observable<Page> {
    const query: QueryDefinition = {distinct: true, filters: filters};
    return this.query(query, size, page, options);
  }

  query(query?: QueryDefinition, size?: number | 20, page?: number | 0,
        options?: {method: 'GET' | 'POST'}): Observable<Page> {
    let params = new HttpParams()
      .set('page', page ? page.toString() : '0')
      .set('size', size ? size.toString() : '20');

    const isGet = options == null || options.method === 'GET';

    if (isGet && query != null && query.filters != null) {
      let opt = '';
      let first = true;
      query.filters.forEach(item => {
        opt += `${item.name + item.operator + item.value}`;
        if (!first) {
          opt = '&' + opt;
        }
        first = false;
      });
      params = params.set('opt', opt);
    }

    if (isGet) {
      return this.http.get<T>(`${this.url}/query`,
        {
          params: params
        });
    } else {
      return this.http.post<T>(`${this.url}/query`, query,
        {
          params: params
        });
    }
  }

  findAllPages(size?: number | 0, page?: number | 20): Observable<Page> {
    return this.http.get<Page>(`${this.url}/pages`,
      {
        params: new HttpParams()
          .set('page', page ? page.toString() : '0')
          .set('size', size ? size.toString() : '20')
      });
  }

}
