import {Condition, ConditionType, MessageConditionType, OperatorType} from '../models/condition.model';
import {Field} from '../models/field.model';
import {FieldType} from '../models/field-type.model';
import {DataFieldLog} from '../models/data-field-log';

export class ConditionUtil {

  static getExpressionType(condition: Condition): string {
    if (condition.type === ConditionType.MESSAGE) {
      switch (condition.expressionType) {
        case MessageConditionType.CONDITION_NAME: {
          return 'mensagem';
        }
        case MessageConditionType.CONDITION_NIVEL: {
          return 'mensagem de nível';
        }
      }
    } else if (condition.type === ConditionType.FIELD) {
      return ` ${condition.field.name}`;
    }
    return '';
  }

  static getAndOr(condition: Condition): string {
    if (condition.andOr) {
      return condition.andOr === 'and' ? 'e' : 'ou';
    }
    return '';
  }

  static getOperatorCondition(condition: Condition): string {
    return this.getOperator(condition.operator);
  }

  static getOperator(operator: string): string {
    if (operator) {
      switch (operator) {
        case OperatorType.EQ: {
          return 'igual a';
        }
        case OperatorType.NE: {
          return 'diferente de';
        }
        case OperatorType.GT: {
          return 'maior que';
        }
        case OperatorType.GTE: {
          return 'maior ou igual a';
        }
        case OperatorType.LT: {
          return 'menor que';
        }
        case OperatorType.LTE: {
          return 'menor ou igual a';
        }
        case OperatorType.LIKE: {
          return 'contém';
        }
        case OperatorType.IS_NULL: {
          return 'é nulo';
        }
        case OperatorType.IS_NOT_NUL: {
          return 'é diferente de nulo';
        }
      }
    }
    return '';
  }

  static getValor(condition: Condition): string {
    if (condition.type === ConditionType.MESSAGE) {
      switch (condition.expressionType) {
        case MessageConditionType.CONDITION_NAME: {
          return 'mensagem';
        }
        case MessageConditionType.CONDITION_NIVEL: {
          return 'mensagem de nível';
        }
      }
    } else if (condition.type === ConditionType.FIELD) {
      return `campo ${condition.field.name}`;
    }
    return '';
  }

  static getExpression(condition: Condition): string {

    if (condition.type === ConditionType.GROUP) {
      return this.getAndOr(condition);
    }

    const expression = `${this.getAndOr(condition)} ${this.getExpressionType(condition)} ${this.getOperatorCondition(condition)}`;
    if (condition.operator !== OperatorType.IS_NULL
      && condition.operator !== OperatorType.IS_NOT_NUL) {
      return `${expression} ${condition.value}`;
    } else {
      return expression;
    }
  }

  static getDataFieldLogExpression(dataFieldLog: DataFieldLog): string {
    const expression = `${dataFieldLog.name} ${this.getOperator(dataFieldLog.operator)}`;
    if (dataFieldLog.operator !== OperatorType.IS_NULL
      && dataFieldLog.operator !== OperatorType.IS_NOT_NUL) {
      return `${expression} ${dataFieldLog.value}`;
    } else {
      return expression;
    }
  }

  static getOperators(field: Field): string[] {
    const operators: string[] = [];

    if (field.type === FieldType.NUMBER) {
      operators.push(OperatorType.EQ);
      operators.push(OperatorType.NE);
      operators.push(OperatorType.GTE);
      operators.push(OperatorType.GT);
      operators.push(OperatorType.LTE);
      operators.push(OperatorType.LT);
    } else if (field.type === FieldType.STRING) {
      operators.push(OperatorType.EQ);
      operators.push(OperatorType.NE);
      operators.push(OperatorType.GTE);
      operators.push(OperatorType.GT);
      operators.push(OperatorType.LTE);
      operators.push(OperatorType.LT);
    } else if (field.type === FieldType.BOOLEAN) {
      operators.push(OperatorType.EQ);
      operators.push(OperatorType.NE);
    }
    operators.push(OperatorType.IS_NULL);
    operators.push(OperatorType.IS_NOT_NUL);

    return operators;
  }

}
