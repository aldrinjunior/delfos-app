import {Component, Input, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

import {AuthService} from '../../../core/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import {ConfiguracaoService} from '../../../core/services/configuracao.service';
import {
  LoginChangePasswordComponent,
  LoginChangePasswordData
} from '../../../login/components/login-change-password/login-change-password.component';
import {User} from '../../../core/models/user.model';


@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.css'],
})
export class DashboardHeaderComponent implements OnInit {

  @Input() sidenav: MatSidenav;

  constructor(
    public title: Title,
    public configService: ConfiguracaoService,
    private authService: AuthService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  get user(): User {
    return this.authService.user;
  }

  changePassword():  void {
    this.dialog.open<LoginChangePasswordComponent, LoginChangePasswordData>(LoginChangePasswordComponent, {
      width: '400px',
      data: {title: 'Alterar senha'}
    });
  }

  onLogout(): void {
    this.authService.logout();
  }

}
