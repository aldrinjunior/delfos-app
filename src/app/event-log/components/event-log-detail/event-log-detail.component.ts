import {Component, Inject, OnInit, Optional} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {DataFieldLog} from '../../../core/models/data-field-log';
import {FormBuilder, FormControl} from '@angular/forms';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, finalize, tap} from 'rxjs/operators';
import {EventLog} from '../../../core/models/event-log.model';
import {EventLogService} from '../../services/event-log.service';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-log-detail.component.html',
  styleUrls: ['./event-log-detail.component.scss']
})
export class EventLogDetailComponent implements OnInit {


  eventLog: EventLog = {
    dataFields: []
  };

  displayedColumns = ['name', 'value'];
  dataSource = new MatTableDataSource<DataFieldLog>();

  filter = new FormControl();
  filteredValues = { name: ''};

  configs = {
    inLoading: false,
    inSaving: false
  };

  constructor(
    @Optional() public dialogRef: MatDialogRef<EventLogDetailComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: EventLogDetailData,
    private formBuilder: FormBuilder,
    private eventLogService: EventLogService,
    private errorService: ErrorService,
  ) { }

  ngOnInit() {
    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }

    this.filter.valueChanges
      .subscribe(value => {
        this.filteredValues['name'] = value;
        this.dataSource.filter = JSON.stringify(this.filteredValues);
      });

    this.dataSource.filterPredicate = this.createFilter();

    if (this.data.eventLog) {
      this.eventLog = this.data.eventLog;

      this.configs.inLoading = true;
      this.eventLogService.findById(this.eventLog.id)
        .pipe(
          tap((data) => {
            this.eventLog = data;
            this.dataSource.data = data.dataFields;
          }),
          catchError((error) => {
            return this.errorService.handleError(error);
          }),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();

      this.dataSource.data = this.eventLog.dataFields;
    }

  }

  private createFilter() {

    const filterFunction = function(data: DataFieldLog, filter): boolean {

      const searchTerms = JSON.parse(filter);

      return data.name.indexOf(searchTerms.name) !== -1;


    };
    return filterFunction;
  }

  onSubmit(): void {
    this.dialogRef.close({});
  }

}

export class EventLogDetailData {
  eventLog: EventLog;
}
