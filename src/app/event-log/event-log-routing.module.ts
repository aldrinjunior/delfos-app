import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EventLogListComponent} from './components/event-log-list/event-log-list.component';
import {AuthGuard} from '../login/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: EventLogListComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventLogRoutingModule { }
