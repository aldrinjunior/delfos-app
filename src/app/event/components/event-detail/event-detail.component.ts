import {Component, Inject, OnInit} from '@angular/core';
import {Event, EventType} from '../../../core/models/event.model';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import {EventService} from '../../services/event.service';
import {ErrorService} from '../../../core/services/error.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Rule} from '../../../core/models/rule.model';
import {RuleSearchComponent} from '../../../rule/components/rule-search/rule-search.component';
import {catchError, finalize, take, tap} from 'rxjs/operators';
import {StateType} from '../../../core/models/state-type';
import {DialogConfirmComponent, DialogConfirmData} from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import {EventUtil} from '../../../core/utils/event-util';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  event: Event = {
    rules: [],
    actions: []
  };

  rulesDisplayedColumns = ['name', 'actions'];
  rulesDataSource = new MatTableDataSource<Rule>();

  formGroup: FormGroup;

  configs = {
    inLoading: false,
    inSaving: false
  };

  constructor(
    private dialogRef: MatDialogRef<EventDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EventDetailData,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private eventService: EventService,
    private errorService: ErrorService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {

    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }
    this.createForm();

    if (this.data.event) {
      this.configs.inLoading = true;

      this.event = this.data.event;
      this.eventService.findDetailById(this.event.id)
        .pipe(
          take(1),
          tap((data) => {
            this.event = data;
            this.rulesDataSource.data = this.event.rules;

            this.formGroup.patchValue({
              ...this.event,
              state: this.event.state === StateType.ENABLED
            });
          }),
          catchError(this.errorService.handleError),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();

      this.formGroup.patchValue({
        ...this.event,
        state: this.event.state === StateType.ENABLED
      });

      if (this.data.view) {
        this.formGroup.get('state').disable();
        this.formGroup.get('allRules').disable();
      }
    }

  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      description: [null],
      type: [null, [Validators.required]],
      retry: [null],
      delay: [null],
      allRules: [false],
      state: [false]
    });
  }

  get inLoading(): boolean {
    return this.configs.inLoading || this.configs.inSaving;
  }

  addRule(): void {
    const dialogRef = this.dialog.open(RuleSearchComponent);

    dialogRef.beforeClosed()
      .subscribe((result) => {
        if (result.data) {

          if (this.event.rules) {
            const rule: Rule = this.event.rules.find((newRule) => newRule.id === result.data.id);
            if (rule) {
              this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
                DialogConfirmComponent,
                { data: {
                    title: 'Atenção',
                    message: `Regra '${rule.name}' já associada ao evento.`,
                    cancelButton: false,
                    primaryButton: 'OK'
                } }
              );
            } else {
              this.event.rules.push(result.data);
              this.rulesDataSource.data = this.event.rules;
            }
          }

        }
      });
  }


  deleteRule(rule: Rule): void {
    const dialogRef = this.dialog.open<DialogConfirmComponent, DialogConfirmData, boolean>(
      DialogConfirmComponent,
      { data: { title: 'Deletar', message: `Deseja excluir a associação com a regra '${rule.name}'?` } }
    );

    dialogRef.beforeClosed()
      .pipe(take(1))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.event.rules.splice(this.event.rules.indexOf(rule), 1);
          this.rulesDataSource.data = this.event.rules;

          this.snackBar.open(`Associação com regra excluída com sucesso.`,
            'OK', {verticalPosition: 'top', duration: 5000});
        }
      });
  }

  getTypeDescription(type: EventType | string): string {
    return EventUtil.getTypeDescription(type);
  }

  onSubmit(): void {
    this.configs.inSaving = true;

    const model: Event = {
      ...this.event,
      ...this.formGroup.value
    };
    model.state = (model.state ? StateType.ENABLED : StateType.DISABLED);

    this.eventService.save(model, model.id)
      .pipe(
        tap((data) => {
          this.configs.inSaving = false;
          this.dialogRef.close({successful: true, data: data});
        }),
        catchError((error) => {
          this.configs.inSaving = false;
          return this.errorService.handleError(error);
        }),
        finalize( () => {
          this.configs.inSaving = false;
          this.configs.inLoading = false;
        })
      ).subscribe();
  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

}

export class EventDetailData {
  title?: string;
  view: boolean | false;
  event?: Event;
}
