import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {Service} from '../../core/services/service';
import {Event} from '../../core/models/event.model';
import {Observable} from 'rxjs';
import {Rule} from '../../core/models/rule.model';

@Injectable({
  providedIn: 'root'
})
export class EventService extends Service<Event> {

  private _http: HttpClient;

  constructor(
    http: HttpClient
  ) {
    super('/api/events', http);
    this._http = http;
  }

  findByNameAndDescription(name: string): Observable<Rule[]> {
    return this._http.get<Rule[]>(`${this.url}/find/name`,
      {
        params: new HttpParams()
          .set('name', name)
      });
  }

}
