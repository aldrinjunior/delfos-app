import {Component, OnInit} from '@angular/core';
import {StatisticService} from '../../services/statistic-service';
import {ErrorService} from '../../../core/services/error.service';
import {catchError, finalize, tap} from 'rxjs/operators';

import {Chart} from 'chart.js';
import {EventLogStatisticView} from '../../../core/models/view/event-log-statistic-view.model';
import {EventType} from '../../../core/models/event.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  countMessages: number | 0;
  countEvents: number | 0;
  countEventByTypeError: number | 0;

  eventTypeChartModel: ChartModel = {
    eventTypes: [],
    counts: [],
    type: 'doughnut',
    options: {
      legend: {
        position: 'bottom'
      }
    },
    done: false
  };

  eventChartModel: ChartModel = {
    eventTypes: [],
    counts: [],
    type: 'doughnut',
    options: {
      legend: {
        position: 'bottom'
      }
    },
    done: false
  };

  eventErrorChartModel: ChartModel = {
    eventTypes: [],
    counts: [],
    type: 'doughnut',
    options: {
      legend: {
        position: 'bottom'
      }
    },
  };

  dateHourEventChartModel: LineModel = {
    options: {
      legend: {
        position: 'bottom'
      }
    },
    type: 'line'
  };

  constructor(
    private statisticService: StatisticService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {

    this.loadSummary();
    this.loadByEventType();
    this.loadByEvent();
    this.loadByEventError();
    this.loadCountEventByDateHour();
   /*
    const load = () => {
      this.loadSummary();
      this.loadByEventType();
      this.loadByEvent();
      this.loadByEventError();
      this.loadCountEventByDateHour();
    };

    setTimeout(load, 500);*/


  }

  private loadSummary(): void {
    this.statisticService.summary()
      .pipe(
        tap(data => {
          if (data.length > 0) {
            data.forEach(item => {
              if (item.viewId === 'messages') {
                this.countMessages = item.count;
              } else if (item.viewId === 'events') {
                this.countEvents = item.count;
              } else if (item.viewId === 'eventTypeError') {
                this.countEventByTypeError = item.count;
              }
            });
          }
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          // this.configs.inLoadingResults = false;
        })
      )
      .subscribe();
  }

  private loadByEventType(): void {

    this.eventTypeChartModel.inLoading = true;

    this.statisticService.countEventLogByEventType()
      .pipe(
        tap((data: EventLogStatisticView[]) => {

          if (data.length > 0) {

            const eventTypes = [];
            const counts = [];

            data.forEach((y: EventLogStatisticView) => {
              eventTypes.push(y.eventType);
              counts.push(y.count);
            });

            this.eventTypeChartModel.eventTypes = eventTypes;
            this.eventTypeChartModel.counts = counts;
            this.eventTypeChartModel.done = true;
          }
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.eventTypeChartModel.inLoading = false;
        })
      )
      .subscribe();
  }

  private loadByEvent(): void {

    this.eventChartModel.inLoading = true;

    this.statisticService.countEventLogByEvent()
      .pipe(
        tap((data: EventLogStatisticView[]) => {

          if (data.length > 0) {

            this.eventChartModel.eventName = [];
            this.eventChartModel.counts = [];

            data.forEach((y: EventLogStatisticView) => {
              this.eventChartModel.eventName.push(y.event.name);
              this.eventChartModel.counts.push(y.count);
            });

            this.eventChartModel.done = true;

          }
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.eventChartModel.inLoading = false;
        })
      )
      .subscribe();
  }

  private loadByEventError(): void {

    this.eventErrorChartModel.inLoading = true;

    this.statisticService.countEventLogByEvent(EventType.ERROR)
      .pipe(
        tap((data: EventLogStatisticView[]) => {

          if (data.length > 0) {

            const eventName = [];
            const counts = [];

            data.forEach((y: EventLogStatisticView) => {
              eventName.push(y.event.name);
              counts.push(y.count);
            });

            this.eventErrorChartModel.eventName = eventName;
            this.eventErrorChartModel.counts = counts;
            this.eventErrorChartModel.done = true;

          }
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.eventErrorChartModel.inLoading = false;
        })
      )
      .subscribe();
  }

  private loadCountEventByDateHour(): void {

    this.dateHourEventChartModel.inLoading = true;

    this.statisticService.countEventLogByDateAndEvent()
      .pipe(
        tap((data: EventLogStatisticView[]) => {

          if (data.length > 0) {

            // this.dateHourEventChartModel.labels = [];
            // this.dateHourEventChartModel.data = [];

            // identifier labels and axisX
            const hours = [];
            const labels = [];
            data.forEach((item: EventLogStatisticView) => {

              const hour = item.dateHour;
              let idx = hours
                .findIndex((x) => x === hour);

              if (idx === -1) {
                hours.push(item.dateHour);
              }

              idx = labels
                .findIndex((label) => label === item.event.name);

              if (idx === -1) {
                labels.push(item.event.name);
              }

            });

            // create graph
            const chartData: any[] = [];
            labels.forEach((label) => {
              const item = {
                data: new Array(hours.length).fill(0),
                label: label
              };
              chartData.push(item);
            });

            // set data axisY
            data.forEach((y: EventLogStatisticView) => {

              const idx = chartData.findIndex(item => {
                return item.label === y.event.name;
              });

              if (idx >= 0) {

                const idxHour = hours.findIndex(hour => {
                  return hour === y.dateHour;
                });

                if (idxHour >= 0) {
                  chartData[idx].data[idxHour] = y.count;
                }

              }

            });

            hours.forEach((item, index) => {
              const dateHour: Date = new Date();
              dateHour.setTime(item);
              hours[index] = `D${dateHour.getDate()}-${dateHour.getHours()}h`;
            });

            this.dateHourEventChartModel.labels = hours;
            this.dateHourEventChartModel.data = chartData;
            this.dateHourEventChartModel.done = true;

          }
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.dateHourEventChartModel.inLoading = false;
        })
      )
      .subscribe();
  }

  // events
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

}

class ChartModel {

  eventName?: string[];
  eventTypes?: string[];
  counts?: number[];
  options?: any = {
    responsive: true,
    legend: {
      position: 'bottom',
      display: true,
      fullWidth: true
    },
    maintainAspectRatio: true
  };
  type: string;
  done?: boolean | false;
  inLoading?: boolean | false;
}

class LineModel {

  data?: any[];
  labels?: any[];
  options?: any = {
    legend: {
      position: 'bottom',
      display: true,
      fullWidth: true
    },
    responsive: true,
    maintainAspectRatio: true
  };
  type: string;
  done?: boolean | false;
  inLoading?: boolean | false;

}
