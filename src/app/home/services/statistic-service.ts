import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SummaryCountView} from '../../core/models/view/summary-count-view';
import {BaseService} from '../../core/services/base.service';
import {EventLogStatisticView} from '../../core/models/view/event-log-statistic-view.model';
import {EventType} from '../../core/models/event.model';

@Injectable({
  providedIn: 'root'
})
export class StatisticService extends BaseService {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  private get url(): string {
    return `${this.getBasePath()}/api/statistic`;
  }

  summary(): Observable<SummaryCountView[]> {
    return this.http.get<SummaryCountView[]>(`${this.url}/summary`);
  }

  countEventLogByEventType(): Observable<EventLogStatisticView[]> {
    return this.http.get<EventLogStatisticView[]>(`${this.url}/count/event-type`);
  }

  countEventLogByEvent(eventType?: EventType | string | null): Observable<EventLogStatisticView[]> {
     if (eventType === null || eventType === undefined) {
      return this.http.get<EventLogStatisticView[]>(`${this.url}/count/event`);
    } else {
      return this.http.get<EventLogStatisticView[]>(`${this.url}/count/event`,
        {
          params: new HttpParams()
            .set('type', eventType)
        });
    }
  }

  countEventLogByDateAndEvent(): Observable<EventLogStatisticView[]> {
    return this.http.get<EventLogStatisticView[]>(`${this.url}/count/date/event`);
  }

}
