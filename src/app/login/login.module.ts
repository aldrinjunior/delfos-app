import {NgModule} from '@angular/core';

import {LoginRoutingModule} from './login-routing.module';
import {LoginComponent} from './components/login/login.component';
import {SharedModule} from '../shared/shared.module';
import { LoginChangePasswordComponent } from './components/login-change-password/login-change-password.component';

@NgModule({
  declarations: [LoginComponent, LoginChangePasswordComponent],
  imports: [
    SharedModule,
    LoginRoutingModule
  ],
  exports: [
    LoginComponent,
    LoginChangePasswordComponent
  ],
  entryComponents: [
    LoginChangePasswordComponent
  ]
})
export class LoginModule { }
