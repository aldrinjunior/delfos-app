import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFieldLogSearchComponent } from './data-field-log-search.component';

describe('DataFieldLogSearchComponent', () => {
  let component: DataFieldLogSearchComponent;
  let fixture: ComponentFixture<DataFieldLogSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFieldLogSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFieldLogSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
