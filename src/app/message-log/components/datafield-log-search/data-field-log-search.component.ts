import {Component, Inject, OnInit} from '@angular/core';
import {Message} from '../../../core/models/message.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {ConditionUtil} from '../../../core/utils/condition-util';
import {FieldType} from '../../../core/models/field-type.model';
import {DataFieldLog} from '../../../core/models/data-field-log';
import {Field} from '../../../core/models/field.model';
import {OperatorType} from '../../../core/models/condition.model';

@Component({
  selector: 'app-datafield-log-search',
  templateUrl: './data-field-log-search.component.html',
  styleUrls: ['./data-field-log-search.component.scss']
})
export class DataFieldLogSearchComponent implements OnInit {

  formGroup: FormGroup;
  operators: any[];

  configs = {
    inLoading: false
  };

  message: Message;
  field: Field = null;

  constructor(
    public dialogRef: MatDialogRef<DataFieldLogSearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataFieldLogSearchData,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.createForm();

    if (this.dialogRef) {
      this.dialogRef.disableClose = true;
    }

    if (this.data) {
      this.message = this.data.message;

      if (this.data.dataField) {
        this.formGroup.patchValue({...this.data.dataField});
        this.changeField(this.data.dataField.fieldId);
        this.changeOperator(this.data.dataField.operator);
      }
    }

  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      operator: [null, Validators.required],
      value: [{value: null, disabled: true}, Validators.required],
     // andOr: ['and', Validators.required],
      fieldId: [null, Validators.required],
    });
  }

  changeField(fieldId): void {
    if (this.message.fields) {
      this.field = this.message.fields.find((data) => data.id === fieldId);
      if (this.field) {
        this.operators = ConditionUtil.getOperators(this.field);

        if (this.formGroup.get('fieldId').value !== '') {
          this.formGroup.get('value').enable();
        } else {
          this.formGroup.get('value').disable();
        }

      } else {
        this.formGroup.get('value').disable();
      }
    }
  }

  changeOperator(value: string): void {
    if (value === OperatorType.IS_NOT_NUL
      || value === OperatorType.IS_NULL) {
      this.formGroup.get('value').disable();
      this.formGroup.get('value').reset();
    } else {
      this.formGroup.get('value').enable();
    }
  }

  /*
   quando utilizado, formGroup.value nao formata corretamente
   o type number
  */
  get inputFieldType(): string {
    if (this.field) {
      if (this.field.type === FieldType.NUMBER
        || this.field.type === 'NUMBER') {
        return 'number';
      }
    }
    return 'text';
  }

  getOperatorDescription(operator: string): string {
    return ConditionUtil.getOperator(operator);
  }

  onConfirm(): void {
    const model: DataFieldLog = {
      ...this.formGroup.value,
      name: this.field.name
    };
    this.dialogRef.close(model);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export class DataFieldLogSearchData {
  title?: string;
  message: Message;
  dataField?: DataFieldLog;
}
