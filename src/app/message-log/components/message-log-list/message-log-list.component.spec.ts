import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageLogListComponent } from './message-log-list.component';

describe('MessageLogListComponent', () => {
  let component: MessageLogListComponent;
  let fixture: ComponentFixture<MessageLogListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageLogListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageLogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
