import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Page} from '../../core/models/page.model';
import {BaseService} from '../../core/services/base.service';
import {MessageLog} from '../../core/models/message-log.model';
import {FilterMessageLogStatistic} from '../../core/models/filters/filter-message-log-statistic';
import {MessageLogStatisticView} from '../../core/models/view/message-log-statistic-view.model';

@Injectable({
  providedIn: 'root'
})
export class MessageLogService extends BaseService {

  private endpointUrl = '/api/message/logs';

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  protected get url(): string {
    return `${this.getBasePath()}${this.endpointUrl}`;
  }

  findById(id: string): Observable<MessageLog> {
    return this.http.get<MessageLog>(`${this.url}/${id}`);
  }

  find(data?: MessageLog, size?: number | 20, page?: number | 0): Observable<Page> {
    return this.http.post<Page>(`${this.url}/find`,
      data,
      {
        params: new HttpParams()
          .set('page', page ? page.toString() : '0')
          .set('size', size ? size.toString() : '20')
      });
  }

  statistic(statisticBy: FilterMessageLogStatistic): Observable<MessageLogStatisticView[]> {
    return this.http.post<MessageLogStatisticView[]>(`${this.url}/statistic`, statisticBy);
  }

}
