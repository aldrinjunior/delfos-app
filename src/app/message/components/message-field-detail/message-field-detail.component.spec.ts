import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageFieldDetailComponent } from './message-field-detail.component';

describe('MessageFieldDetailComponent', () => {
  let component: MessageFieldDetailComponent;
  let fixture: ComponentFixture<MessageFieldDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageFieldDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageFieldDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
