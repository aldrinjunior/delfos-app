import {Component, OnInit, Optional} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {FormControl} from '@angular/forms';
import {MessageService} from '../../services/message.service';
import {ErrorService} from '../../../core/services/error.service';
import {Message} from '../../../core/models/message.model';
import {catchError, debounceTime, distinctUntilChanged, filter, finalize, tap} from 'rxjs/operators';

@Component({
  selector: 'app-search-message',
  templateUrl: './message-search.component.html',
  styleUrls: ['./message-search.component.scss']
})
export class MessageSearchComponent implements OnInit {

  displayedColumns = ['name', 'description', 'actions'];
  dataSource = new MatTableDataSource<Message>();

  filter = new FormControl();

  configs = {
    inLoading: false,
    resultSearch: false
  };

  message: Message = null;

  constructor(
    @Optional() public dialogRef: MatDialogRef<MessageSearchComponent>,
    private messageService: MessageService,
    private errorService: ErrorService,
  ) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;

    this.filter.valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        filter((query: string) => query && query.length > 2),
        tap((value) => {
          this.configs.inLoading = true;
          this.messageService.search(value)
            .pipe(
              tap((data) => {
                this.dataSource.data = data;
                this.configs.resultSearch = true;
              }),
              catchError((error) => {
                return this.errorService.handleError(error);
              }),
              finalize(() => {
                this.configs.inLoading = false;
              })
            ).subscribe();
        })
      ).subscribe();

  }

  select(message: Message): void {
    this.message = message;
  }

  onSelected(message?: Message): void {
    this.dialogRef.close({data: message ? message : this.message});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

