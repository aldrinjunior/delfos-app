import {NgModule} from '@angular/core';

import {MessageRoutingModule} from './message-routing.module';
import {MessageListComponent} from './components/message-list/message-list.component';
import {SharedModule} from '../shared/shared.module';
import { MessageDetailComponent } from './components/message-detail/message-detail.component';
import { MessageFieldDetailComponent } from './components/message-field-detail/message-field-detail.component';
import {DialogConfirmComponent} from '../shared/components/dialog-confirm/dialog-confirm.component';
import { MessageSearchComponent } from './components/message-search/message-search.component';

@NgModule({
  declarations: [MessageListComponent, MessageDetailComponent, MessageFieldDetailComponent, MessageSearchComponent],
  imports: [
    SharedModule,
    MessageRoutingModule
  ],
  exports: [MessageSearchComponent, MessageDetailComponent],
  entryComponents: [DialogConfirmComponent, MessageDetailComponent, MessageFieldDetailComponent, MessageSearchComponent]
})
export class MessageModule { }
