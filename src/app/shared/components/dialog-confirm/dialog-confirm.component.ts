import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.css']
})
export class DialogConfirmComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogConfirmData,
    private dialogRef: MatDialogRef<DialogConfirmComponent>
  ) { }

  ngOnInit(): void {
    this.dialogRef.disableClose = (this.data.disableClose !== undefined) ? this.data.disableClose : true;
    this.data.cancelButton = (this.data.cancelButton !== undefined) ? this.data.cancelButton : true;
  }

}

export class DialogConfirmData {
  title: string;
  message: string;
  disableClose?: boolean;
  cancelButton?: boolean;

  primaryButton?: string;
  secondaryButton?: string;

}
