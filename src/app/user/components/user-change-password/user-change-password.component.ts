import { Component, Inject, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {AuthService} from '../../../core/services/auth.service';
import {UserService} from '../../services/user.service';
import {ErrorService} from '../../../core/services/error.service';
import {Observable} from 'rxjs';
import {catchError, finalize, tap} from 'rxjs/operators';
import {User} from '../../../core/models/user.model';

@Component({
  selector: 'app-user-change-password',
  templateUrl: './user-change-password.component.html',
  styleUrls: ['./user-change-password.component.scss']
})
export class UserChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;

  configs = {
    inLoading: false,
    inSaving: false,
    hidePassword: true,
    confirmPassword: false
  };

  constructor(
    public dialogRef: MatDialogRef<UserChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserChangePasswordData,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private userService: UserService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {

    this.dialogRef.disableClose = true;
    this.createForm();

  }

  private createForm(): void {

    this.changePasswordForm = this.formBuilder.group({
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])],
      confirmNewPassword: ['', [Validators.required]]
    });

    if (!this.data.user) {
      this.changePasswordForm.addControl('password',
        new FormControl('', [Validators.required]));
    }

  }

  onValidatePassword(): void {

    const passwordControl = this.changePasswordForm.get('newPassword');
    const confirmPasswordControl = this.changePasswordForm.get('confirmNewPassword');

    if (passwordControl == null || confirmPasswordControl == null) {
      this.configs.confirmPassword = true;
      return;
    }

    this.configs.confirmPassword = (passwordControl.value === confirmPasswordControl.value);

    const control = this.changePasswordForm.get('confirmNewPassword');
    if (this.configs.confirmPassword) {
      control.setErrors(null);
    } else {
      control.setErrors({'confirmPassword': true});
    }

  }

  onSubimit(): void {

    this.onValidatePassword();
    if (!this.configs.confirmPassword) {
      return;
    }

    this.configs.inSaving = true;
    const changePassword = this.changePasswordForm.value;

    let operation: Observable<any>;
    if (this.data.user) {
      operation = this.changePassword(this.data.user, changePassword.newPassword);
    } else {
      operation = this.changePasswordCurrentUser(changePassword.password, changePassword.newPassword);
    }

    operation.pipe(
      tap(() => {
        this.snackBar.open(`Alteração realizada com sucesso.`,
          'OK', {verticalPosition: 'top', duration: 5000});
        this.dialogRef.close({successful: true});
      }),
      catchError((error) => {
        return this.errorService.handleError(error);
      }),
      finalize(() => {
        this.configs.inSaving = false;
        this.configs.inLoading = false;
      })
    ).subscribe();

  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

  private changePasswordCurrentUser(password: string, newPassword: string): Observable<any> {
    return this.authService.changePasswordCurrentUser(password, newPassword);
  }

  private changePassword(user: User, password: string): Observable<any> {
    return this.userService.changePassword(user, password);
  }

}

export class UserChangePasswordData {
  title?: string;
  user?: User;
}
