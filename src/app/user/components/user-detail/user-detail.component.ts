import {Component, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UserService} from '../../services/user.service';
import {ErrorService} from '../../../core/services/error.service';
import {AuthService} from '../../../core/services/auth.service';

import {User} from '../../../core/models/user.model';
import {catchError, finalize, tap} from 'rxjs/operators';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  user: User = {id: null};
  formGroup: FormGroup;

  configs = {
    inLoading: false,
    inSaving: false,
    hidePassword: true,
    confirmPassword: false
  };


  constructor(
    public dialogRef: MatDialogRef<UserDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserDetailData,
    private userService: UserService,
    private authService: AuthService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.dialogRef.disableClose = true;
    this.createForm();

    if (this.data.user) {
      this.user = this.data.user;

      this.configs.inLoading = true;
      this.userService.findById(this.user.id)
        .pipe(
          tap((data) => {
            this.user = data;
            this.formGroup.patchValue({...this.user});
          }),
          catchError((error) => this.errorService.handleError(error)),
          finalize(() => {
            this.configs.inLoading = false;
          })
        ).subscribe();

      this.formGroup.patchValue({...this.user});
    }

    if (!this.user.id) {
      this.formGroup.addControl('password',
        new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]));
      this.formGroup.addControl('confirmPassword',
        new FormControl('', [Validators.required]));
    }

  }

  get authorizedUser(): User {
    return this.authService.user;
  }

  private createForm(): void {
    const userDisabled = false; // !!this.data.user;
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      username: [{value: '', disabled: userDisabled}, [Validators.required, Validators.minLength(2)]]
    });

  }

  onValidatePassword(): void {

    const passwordControl = this.formGroup.get('password');
    const confirmPasswordControl = this.formGroup.get('confirmPassword');

    if (passwordControl == null || confirmPasswordControl == null) {
      this.configs.confirmPassword = true;
      return;
    }

    this.configs.confirmPassword = (passwordControl.value === confirmPasswordControl.value);

    const control = this.formGroup.get('confirmPassword');
    if (this.configs.confirmPassword) {
      control.setErrors(null);
    } else {
      control.setErrors({'confirmPassword': true});
    }

  }

  onSubmit(): void {

    this.onValidatePassword();
    if (!this.configs.confirmPassword) {
      return;
    }

    this.configs.inSaving = true;
    const model: User = {
      id: this.user.id,
      ...this.formGroup.value
    };

    if (this.user.username === 'admin'
      && model.username !== this.user.username) {
      if (model.username === '') {
        model.username = null;
      }
    }

    this.userService.save(model)
      .pipe(
        tap((data) => {
          this.dialogRef.close({successful: true, data: data});
        }),
        catchError((error) => {
          return this.errorService.handleError(error);
        }),
        finalize(() => {
          this.configs.inSaving = false;
          this.configs.inLoading = false;
        })
      ).subscribe();
  }

  onNoClick(): void {
    this.dialogRef.close({});
  }

}

export class UserDetailData {
  title?: string;
  user?: User;
}

export const passwordMatcher = (control: AbstractControl): {[key: string]: boolean} => {

  const email = control.get('password');
  const confirm = control.get('confirmPassword');
  if (!email || !confirm) {
    return null;
  } else {
    return email.value === confirm.value ? null : { nomatch: true };
  }

};
