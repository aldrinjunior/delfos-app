import { Injectable } from '@angular/core';
import {Service} from '../../core/services/service';
import {User} from '../../core/models/user.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends Service<User> {

  private _http: HttpClient;

  constructor(
    http: HttpClient
  ) {
    super('/api/users', http);
    this._http = http;
  }

  changePassword(user: User, password: string): Observable<any> {
    return this._http.post(`${this.url}/${user.id}/change/password`,
      {user: user, password: password});
  }

}
