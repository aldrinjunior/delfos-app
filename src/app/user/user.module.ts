import {NgModule} from '@angular/core';

import {UserRoutingModule} from './user-routing.module';
import {UserListComponent} from './components/user-list/user-list.component';
import {SharedModule} from '../shared/shared.module';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import {DialogConfirmComponent} from '../shared/components/dialog-confirm/dialog-confirm.component';
import { UserChangePasswordComponent } from './components/user-change-password/user-change-password.component';

@NgModule({
  declarations: [UserListComponent, UserDetailComponent, UserChangePasswordComponent],
  imports: [
    SharedModule,
    UserRoutingModule
  ],
  entryComponents: [
    UserDetailComponent,
    UserChangePasswordComponent,
    DialogConfirmComponent
  ]
})
export class UserModule { }
